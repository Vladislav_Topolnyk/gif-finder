package com.vladtop.test_application.presentation.fragments.Garbage

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.Glide
import com.vladtop.test_application.data.repository.GifsRepository
import com.vladtop.test_application.domain.Gif
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class GarbageViewModel @Inject constructor(
    application: Application,
    private val repository: GifsRepository
) : AndroidViewModel(application) {
    private val _removedGifs: MutableLiveData<List<Gif>> = MutableLiveData()
    private val applicationContext = getApplication<Application>()
    val removedGifs: LiveData<List<Gif>> = _removedGifs

    fun getRemovedGifs() = viewModelScope.launch {
        _removedGifs.value = repository.getGifs(isRemoved = true)
    }

    fun updateIsRemoved(id: String, isRemoved: Boolean) = viewModelScope.launch {
        repository.updateIsRemoved(id, isRemoved)
    }

    fun deleteGif(id: String) = viewModelScope.launch {
        repository.deleteGif(id)
    }

    fun deleteFileFromStorage(id: String, url: String) = viewModelScope.launch {
        val file = getFileByUrl(url).absoluteFile
        repository.updateIsRemovedForever(id, true)
        file.delete()
    }

    private suspend fun getFileByUrl(url: String) = withContext(Dispatchers.IO) {
        Glide.with(applicationContext)
            .asFile()
            .load(url)
            .submit().get()
    }

}