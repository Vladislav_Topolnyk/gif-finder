package com.vladtop.test_application.presentation.adapters.ViewPagerAdapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.vladtop.test_application.databinding.ViewPagerItemBinding
import com.vladtop.test_application.domain.Gif

class ViewPagerAdapter(
    private val gifItemList: List<Gif>,
    private val onBackClickListener: OnBackClickListener
) : RecyclerView.Adapter<ViewPagerAdapter.VPViewHolder>(

) {

    interface OnBackClickListener {
        fun onBackClicked()
    }

    inner class VPViewHolder(private val binding: ViewPagerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(gif: Gif) {
            binding.backBtn.setOnClickListener {
                onBackClickListener.onBackClicked()
            }

            Glide.with(binding.root)
                .load(gif.url)
                .fitCenter()
                .placeholder(android.R.drawable.ic_menu_upload)
                .error(android.R.drawable.stat_notify_error)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.gifImageView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VPViewHolder {
        val binding = ViewPagerItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return VPViewHolder(binding)
    }

    override fun getItemCount(): Int = gifItemList.size

    override fun onBindViewHolder(holder: VPViewHolder, position: Int) {
        holder.bind(gifItemList[position])
    }
}