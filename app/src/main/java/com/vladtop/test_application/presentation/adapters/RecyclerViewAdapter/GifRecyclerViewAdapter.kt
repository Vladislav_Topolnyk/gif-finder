package com.vladtop.test_application.presentation.adapters.RecyclerViewAdapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.vladtop.test_application.databinding.CardViewGifBinding
import com.vladtop.test_application.domain.Gif

class GifRecyclerViewAdapter(
    private val gifItemList: List<Gif>,
    private val onItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<GifRecyclerViewAdapter.RVViewHolder>(

) {
    interface OnItemClickListener {
        fun onItemClick(gif: Gif)
        fun onDeleteClick(position: Int)
    }

    inner class RVViewHolder(private val binding: CardViewGifBinding) :
        RecyclerView.ViewHolder(binding.root) {

        val removeButton: ImageButton = binding.deleteBtn

        fun bind(gif: Gif) {
            binding.root.setOnClickListener {
                onItemClickListener.onItemClick(gif)
            }

            Glide.with(binding.root)
                .load(gif.url)
                .centerCrop()
                .placeholder(android.R.drawable.ic_menu_upload)
                .error(android.R.drawable.stat_notify_error)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.gifImageView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RVViewHolder {
        val binding = CardViewGifBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return RVViewHolder(binding)
    }

    override fun getItemCount(): Int = gifItemList.size

    override fun onBindViewHolder(holder: RVViewHolder, position: Int) {
        holder.bind(gifItemList[position])

        holder.removeButton.setOnClickListener {
            onItemClickListener.onDeleteClick(position)
        }
    }
}