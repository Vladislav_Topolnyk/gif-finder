package com.vladtop.test_application.presentation.fragments.Gif

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.google.android.material.tabs.TabLayoutMediator
import com.vladtop.test_application.R
import com.vladtop.test_application.data.mappers.GifListConverter
import com.vladtop.test_application.databinding.FragmentGifBinding
import com.vladtop.test_application.domain.Gif
import com.vladtop.test_application.presentation.fragments.Search.SearchFragment
import com.vladtop.test_application.presentation.adapters.ViewPagerAdapter.ViewPagerAdapter
import com.vladtop.test_application.presentation.adapters.ViewPagerAdapter.ZoomOutPageTransformer
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class GifFragment : Fragment(), ViewPagerAdapter.OnBackClickListener {

    private lateinit var binding: FragmentGifBinding
    private val gifList = mutableListOf<Gif>()

    @Inject
    lateinit var gifListConverter: GifListConverter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            val json = it.getString(SearchFragment.GIF_LIST)
            val selectedPosition = it.getInt(SearchFragment.SELECTED_POSITION)
            if (json != null) {
                gifList.addAll(gifListConverter.fromJson(json))
                moveToFirst(selectedPosition)
            }

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentGifBinding.inflate(inflater, container, false)
        provideViewPager()

        return binding.root
    }

    private fun provideViewPager() {
        val viewPager = binding.viewPager
        viewPager.adapter = ViewPagerAdapter(gifList, this)
        viewPager.setPageTransformer(ZoomOutPageTransformer())

        provideTabLayout()
    }

    private fun provideTabLayout() {
        val tabLayout = binding.tabLayout
        TabLayoutMediator(tabLayout, binding.viewPager) { tab, p ->
            tab.text = "Gif ${(p + 1)}"
        }.attach()
    }

    override fun onBackClicked() {
        navigateToSearchFragment()
    }

    private fun navigateToSearchFragment() {
        findNavController().popBackStack()
    }

    private fun moveToFirst(position: Int) {
        val mainGif = gifList[position]
        gifList.removeAt(position)
        gifList.add(0, mainGif)
    }

}