package com.vladtop.test_application.presentation.fragments.Search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vladtop.test_application.data.repository.GifsRepository
import com.vladtop.test_application.domain.Gif
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val repository: GifsRepository
) : ViewModel() {
    private val _errorData: MutableLiveData<Boolean> = MutableLiveData()
    private val _gifs: MutableLiveData<List<Gif>> = MutableLiveData()
    val gifs: LiveData<List<Gif>> = _gifs
    val errorData: LiveData<Boolean> = _errorData

    fun getGifs(query: String) = viewModelScope.launch {
        val isSuccessful = repository.putGifs(query)
        if (isSuccessful) {
            _gifs.value = repository.getGifs(false)
        } else {
            _errorData.value = true
        }
    }

    fun updateIsRemoved(id: String, isRemoved: Boolean) = viewModelScope.launch {
        repository.updateIsRemoved(id, isRemoved)
    }

    fun deleteAll(query: String) = viewModelScope.launch {
        repository.deleteAll(query)
    }

}