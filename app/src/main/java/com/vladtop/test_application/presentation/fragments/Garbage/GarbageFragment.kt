package com.vladtop.test_application.presentation.fragments.Garbage

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.vladtop.test_application.R
import com.vladtop.test_application.databinding.FragmentGarbageBinding
import com.vladtop.test_application.domain.Gif
import com.vladtop.test_application.presentation.adapters.RecyclerViewAdapter.RemovedGifRecyclerViewAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GarbageFragment : Fragment(), RemovedGifRecyclerViewAdapter.OnItemClickListener {

    private lateinit var binding: FragmentGarbageBinding
    private val garbageViewModel: GarbageViewModel by viewModels()
    private val gifList = mutableListOf<Gif>()
    private lateinit var adapterGif: RemovedGifRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentGarbageBinding.inflate(inflater, container, false)

        observeData()
        provideRecyclerView()
        onClickEvents()

        return binding.root
    }

    private fun onClickEvents() {
        onBackClicked()
    }

    private fun observeData() {
        garbageViewModel.removedGifs.observe(viewLifecycleOwner, ::processGifs)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun processGifs(gifs: List<Gif>) {
        gifList.addAll(gifs)
        adapterGif.notifyDataSetChanged()
    }

    private fun provideRecyclerView() {
        val recyclerView = binding.gifsRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        adapterGif = RemovedGifRecyclerViewAdapter(requireContext(), gifList, this)
        recyclerView.adapter = adapterGif
        garbageViewModel.getRemovedGifs()
    }

    private fun onBackClicked() {
        binding.backBtn.setOnClickListener {
            navigateToSearchFragment()
        }
    }

    private fun navigateToSearchFragment() {
        findNavController().popBackStack()
    }


    private fun removeFromList(position: Int) {
        gifList.removeAt(position)
        adapterGif.notifyItemRemoved(position)
        adapterGif.notifyItemRangeChanged(position, gifList.size)
    }

    private fun deleteFromStorage(id: String, url: String) {
        garbageViewModel.deleteFileFromStorage(id, url)
    }

    override fun onDeleteClick(position: Int) {
        val currentGif = gifList[position]
        deleteFromStorage(currentGif.id, currentGif.url)
        removeFromList(position)
        showDeletedForeverMessage()
    }

    override fun onRestoreCLick(position: Int) {
        val currentGif = gifList[position]
        garbageViewModel.updateIsRemoved(currentGif.id, false)
        garbageViewModel.deleteGif(currentGif.id)
        removeFromList(position)
        showRestoredMessage()
    }

    private fun showRestoredMessage() {
        Toast.makeText(requireContext(), "Gif restored to the next requests!", Toast.LENGTH_SHORT)
            .show()
    }

    private fun showDeletedForeverMessage() {
        Toast.makeText(requireContext(), "Gif deleted forever!", Toast.LENGTH_SHORT).show()
    }

}