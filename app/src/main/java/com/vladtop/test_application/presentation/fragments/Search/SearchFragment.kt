package com.vladtop.test_application.presentation.fragments.Search

import android.annotation.SuppressLint
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.vladtop.test_application.R
import com.vladtop.test_application.data.mappers.GifListConverter
import com.vladtop.test_application.databinding.FragmentSearchBinding
import com.vladtop.test_application.domain.Gif
import com.vladtop.test_application.presentation.adapters.RecyclerViewAdapter.GifRecyclerViewAdapter
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

const val LAST_QUERY_PREF = "last_query_pref"
const val QUERY_KEY = "query"

@AndroidEntryPoint
class SearchFragment : Fragment(),
    GifRecyclerViewAdapter.OnItemClickListener {

    private lateinit var binding: FragmentSearchBinding
    private val searchViewModel: SearchViewModel by viewModels()
    private val gifList = mutableListOf<Gif>()
    private lateinit var adapterGif: GifRecyclerViewAdapter
    private var previousQuery: String = ""
    private lateinit var preferences: SharedPreferences

    @Inject
    lateinit var gifListConverter: GifListConverter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchBinding.inflate(inflater, container, false)

        preferences = requireActivity().getSharedPreferences(LAST_QUERY_PREF, MODE_PRIVATE)
        previousQuery = preferences.getString(QUERY_KEY, "") ?: ""

        observeData()
        provideRecyclerView()
        onClickEvents()

        return binding.root
    }

    override fun onStop() {
        super.onStop()
        preferences.edit().putString(QUERY_KEY,previousQuery).apply()
    }

    private fun observeData() {
        observeSuccessData()
        observeErrorData()
    }

    private fun onClickEvents() {
        onSearchClicked()
        fabOnClicked()
    }

    private fun observeErrorData() {
        searchViewModel.errorData.observe(viewLifecycleOwner, ::processErrorData)
    }

    private fun observeSuccessData() {
        searchViewModel.gifs.observe(viewLifecycleOwner, ::processGifs)
    }

    private fun onSearchClicked() {
        binding.searchBtn.setOnClickListener {
            val input = binding.editTxt.text.toString()

            val query = formatQuery(input)

            if (wrongInput(query)) {
                wrongInputCase()
            } else {
                clearList()
                searchViewModel.deleteAll(previousQuery)
                searchViewModel.getGifs(query)
                setSearchButtonEnabled(false)
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun clearList() {
        gifList.clear()
        adapterGif.notifyDataSetChanged()
    }

    private fun formatQuery(query: String) =
        query.trimStart().trimEnd().lowercase(Locale.getDefault())

    private fun setSearchButtonEnabled(isEnabled: Boolean) {
        binding.searchBtn.isEnabled = isEnabled
    }

    private fun wrongInput(query: String) =
        query.trim() == ""

    private fun provideRecyclerView() {
        val recyclerView = binding.gifsRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        adapterGif = GifRecyclerViewAdapter(gifList, this)
        recyclerView.adapter = adapterGif
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun processGifs(gifs: List<Gif>) {

        if (gifs.isEmpty()) return

        val currentQuery = gifs[0].q
        previousQuery = currentQuery

        gifList.addAll(gifs)
        adapterGif.notifyDataSetChanged()

        setSearchButtonEnabled(true)
    }

    private fun processErrorData(error: Boolean) {
        if (error) {
            showErrorMessage()
            setSearchButtonEnabled(true)
        }
    }

    private fun showErrorMessage() {
        Toast.makeText(
            requireContext(),
            "We can`t provide gifs for this input!",
            Toast.LENGTH_SHORT
        )
            .show()
    }


    override fun onItemClick(gif: Gif) {
        val bundle = bundleToGifFragment(gif)
        navigateToGifFragment(bundle)
    }

    private fun bundleToGifFragment(gif: Gif): Bundle = bundleOf(
        GIF_LIST to gifListConverter.toJson(gifList),
        SELECTED_POSITION to gifList.indexOf(gif)
    )

    private fun navigateToGifFragment(bundle: Bundle) {
        findNavController().navigate(
            R.id.action_searchFragment_to_gifFragment,
            bundle
        )
    }

    override fun onDeleteClick(position: Int) {
        searchViewModel.updateIsRemoved(gifList[position].id, true)
        deleteFromList(position)
        showAddToBucketMessage()
    }

    private fun deleteFromList(position: Int) {
        gifList.removeAt(position)
        adapterGif.notifyItemRemoved(position)
        adapterGif.notifyItemRangeChanged(position, gifList.size)
    }

    private fun showAddToBucketMessage() {
        Toast.makeText(requireContext(), "Added to bucket!", Toast.LENGTH_SHORT).show()
    }

    private fun wrongInputCase() {
        binding.editTxt.text.clear()
        Toast.makeText(
            requireContext(),
            "Wrong input!",
            Toast.LENGTH_SHORT
        )
            .show()
    }

    private fun fabOnClicked() {
        binding.fab?.setOnClickListener {
            navigateToGarbageFragment()
        }
    }

    private fun navigateToGarbageFragment() {
        findNavController().navigate(R.id.action_searchFragment_to_garbageFragment)
    }

    companion object {
        const val SELECTED_POSITION = "main gif"
        const val GIF_LIST = "Gif list"
    }
}