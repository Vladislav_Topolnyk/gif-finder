package com.vladtop.test_application.presentation.adapters.RecyclerViewAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vladtop.test_application.R
import com.vladtop.test_application.databinding.CardViewRemovedGifBinding
import com.vladtop.test_application.domain.Gif

class RemovedGifRecyclerViewAdapter(
    private val context: Context,
    private val gifList: List<Gif>,
    private val onItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<RemovedGifRecyclerViewAdapter.RemovedGifRVViewHolder>(

) {

    interface OnItemClickListener {
        fun onDeleteClick(position: Int)
        fun onRestoreCLick(position: Int)
    }

    inner class RemovedGifRVViewHolder(private val binding: CardViewRemovedGifBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private val menuBtn: ImageButton = binding.dropdownMenu

        fun bind(position: Int) {
            loadGifFromCache(position)

            menuBtn.setOnClickListener {
                provideMenu(position)
            }
        }

        private fun loadGifFromCache(position: Int) {
            Glide.with(binding.root)
                .load(gifList[position].url)
                .centerCrop()
                .placeholder(android.R.drawable.ic_menu_upload)
                .error(android.R.drawable.stat_notify_error)
                .onlyRetrieveFromCache(true)
                .into(binding.gifImageView)
        }

        private fun provideMenu(position: Int) {
            val popupMenu =
                PopupMenu(context, menuBtn)

            popupMenu.inflate(R.menu.gif_item_menu)
            popupMenu.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem?): Boolean {
                    when (item?.itemId) {
                        R.id.restore_item -> {
                            onItemClickListener.onRestoreCLick(position)
                            return true
                        }
                        R.id.delete_forever_item -> {
                            onItemClickListener.onDeleteClick(position)
                            return true
                        }
                    }
                    return false
                }
            })
            popupMenu.show()
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RemovedGifRVViewHolder {
        val binding = CardViewRemovedGifBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return RemovedGifRVViewHolder(binding)
    }

    override fun getItemCount(): Int = gifList.size

    override fun onBindViewHolder(holder: RemovedGifRVViewHolder, position: Int) {
        holder.bind(position)
    }
}
