package com.vladtop.test_application.data.api.DTO

import com.google.gson.annotations.SerializedName

data class ImagesDTO(
    @SerializedName("original") var original: OriginalDTO = OriginalDTO()
)
