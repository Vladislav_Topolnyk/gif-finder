package com.vladtop.test_application.data.mappers

import com.vladtop.test_application.data.api.DTO.GifsDTO
import com.vladtop.test_application.domain.Gif
import retrofit2.Response
import javax.inject.Inject

class GifsMapper @Inject constructor() {
    fun transform(response: Response<GifsDTO>): List<Gif>? {
        return if (response.isSuccessful) {
            val gifList = response.toList()
            gifList.ifEmpty { null }
        } else null
    }

    private fun Response<GifsDTO>.toList(): List<Gif> {
        val list = mutableListOf<Gif>()
        body()?.data?.forEach { gif ->
            list.add(
                Gif(
                    id = gif.id,
                    url = gif.images.original.url
                )
            )
        }
        return list
    }
}