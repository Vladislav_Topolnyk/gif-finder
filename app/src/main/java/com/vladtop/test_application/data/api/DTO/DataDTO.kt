package com.vladtop.test_application.data.api.DTO

import com.google.gson.annotations.SerializedName

data class DataDTO(
    @SerializedName("images") var images: ImagesDTO = ImagesDTO(),
    @SerializedName("id") var id: String = "",
)
