package com.vladtop.test_application.data.repository

import com.vladtop.test_application.data.api.GifsApi
import com.vladtop.test_application.data.mappers.GifsMapper
import com.vladtop.test_application.data.room.dao.GifsDao
import com.vladtop.test_application.domain.Gif
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GifsRepository @Inject constructor(
    private val gifsApi: GifsApi,
    private val mapper: GifsMapper,
    private val gifsDao: GifsDao
) {
    private suspend fun getGifsFromApi(query: String, limit: Int): List<Gif>? =
        gifsApi.getGifs(query = query, limit = limit)
            .let(mapper::transform)

    suspend fun putGifs(query: String): Boolean = withContext(Dispatchers.IO) {

        val isRemovedList = gifsDao.getIsRemovedItems(query)
        val limit = GifsApi.DEFAULT_LIMIT + isRemovedList.size

        val gifs: List<Gif>? = getGifsFromApi(query, limit)
        if (gifs != null) {
            val listGifEntity = toListGifEntity(gifs, query)
            gifsDao.insertAll(listGifEntity)
            true
        } else false
    }

    private fun toListGifEntity(gifs: List<Gif>, query: String) = gifs.map {
        it.q = query
        it.toGifEntity()
    }

    suspend fun getGifs(isRemoved: Boolean): List<Gif> = withContext(Dispatchers.IO) {
        val list = gifsDao.getAll(isRemoved)
        list
    }

    suspend fun deleteAll(query: String) = withContext(Dispatchers.IO) {
        gifsDao.deleteAll(query)
    }

    suspend fun updateIsRemoved(id: String, isRemoved: Boolean) = withContext(Dispatchers.IO) {
        gifsDao.updateIsRemoved(id, isRemoved)
    }

    suspend fun updateIsRemovedForever(id: String, isRemovedForever: Boolean) =
        withContext(Dispatchers.IO) {
            gifsDao.updateIsRemovedForever(id, isRemovedForever)
        }

    suspend fun deleteGif(id: String) = withContext(Dispatchers.IO) {
        gifsDao.deleteGifById(id)
    }
}