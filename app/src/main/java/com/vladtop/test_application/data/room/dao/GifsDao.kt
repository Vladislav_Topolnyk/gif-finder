package com.vladtop.test_application.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.vladtop.test_application.data.room.entities.GifsEntity
import com.vladtop.test_application.domain.Gif

@Dao
interface GifsDao {
    @Insert(entity = GifsEntity::class, onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(list: List<GifsEntity>)

    @Query("SELECT * FROM ${GifsEntity.TABLE_NAME} WHERE isRemoved = :isRemoved AND isRemovedForever = :isRemovedForever")
    suspend fun getAll(isRemoved: Boolean, isRemovedForever: Boolean = false): List<Gif>

    @Query("DELETE FROM ${GifsEntity.TABLE_NAME} WHERE isRemoved = :isRemoved AND q = :query")
    suspend fun deleteAll(query: String, isRemoved: Boolean = false)

    @Query("UPDATE ${GifsEntity.TABLE_NAME} SET isRemoved = :isRemoved WHERE id = :id")
    suspend fun updateIsRemoved(id: String, isRemoved: Boolean)

    @Query("SELECT * FROM ${GifsEntity.TABLE_NAME} WHERE q = :query AND isRemoved = :isRemoved")
    suspend fun getIsRemovedItems(query: String, isRemoved: Boolean = true): List<Gif>

    @Query("UPDATE ${GifsEntity.TABLE_NAME} SET isRemovedForever = :isRemovedForever WHERE id = :id")
    suspend fun updateIsRemovedForever(id: String, isRemovedForever: Boolean)

    @Query("DELETE FROM ${GifsEntity.TABLE_NAME} WHERE id = :id")
    suspend fun deleteGifById(id: String)

}