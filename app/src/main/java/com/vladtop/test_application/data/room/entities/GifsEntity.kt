package com.vladtop.test_application.data.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = GifsEntity.TABLE_NAME
)
data class GifsEntity(
    @PrimaryKey
    val id: String,
    @ColumnInfo
    val url: String,
    @ColumnInfo
    val q: String,
    @ColumnInfo(defaultValue = false.toString())
    val isRemoved: Boolean,
    @ColumnInfo(defaultValue = false.toString())
    val isRemovedForever: Boolean
) {
    companion object {
        const val TABLE_NAME = "gifs_entity"
    }
}