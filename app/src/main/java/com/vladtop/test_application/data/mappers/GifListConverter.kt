package com.vladtop.test_application.data.mappers

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vladtop.test_application.domain.Gif
import javax.inject.Inject

class GifListConverter @Inject constructor() {
    fun toJson(list: List<Gif>): String {
        val gson = Gson()
        val type = object : TypeToken<List<Gif>>() {}.type
        return gson.toJson(list, type)
    }

    fun fromJson(json: String): List<Gif> {
        val gson = Gson()
        val type = object : TypeToken<List<Gif>>() {}.type
        return gson.fromJson(json, type)
    }
}