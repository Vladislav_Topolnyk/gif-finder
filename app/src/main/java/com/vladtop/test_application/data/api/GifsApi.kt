package com.vladtop.test_application.data.api

import com.vladtop.test_application.data.api.DTO.GifsDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface GifsApi {
    companion object {
        const val BASE_URL = "https://api.giphy.com/v1/gifs/"
        private const val API_KEY = "YGHnKKBGSydS6nSt6WAoUcICWwmgCfvL"
        const val DEFAULT_LIMIT = 7
    }

    @GET("search?api_key=$API_KEY")
    suspend fun getGifs(
        @Query("q") query: String,
        @Query("limit") limit: Int = 7,
        @Query("offset") offset: Int = 0,
        @Query("rating") rating: String = "g",
        @Query("lang") language: String = "en"
    ): Response<GifsDTO>
}