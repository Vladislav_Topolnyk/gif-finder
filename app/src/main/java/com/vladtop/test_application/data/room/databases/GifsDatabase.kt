package com.vladtop.test_application.data.room.databases

import androidx.room.Database
import androidx.room.RoomDatabase
import com.vladtop.test_application.data.room.dao.GifsDao
import com.vladtop.test_application.data.room.entities.GifsEntity

@Database(
    entities = [
        GifsEntity::class
    ],
    version = 1,
)
abstract class GifsDatabase : RoomDatabase() {

    companion object {
        const val DB_NAME = "gifs.db"
    }

    abstract fun forecastDao(): GifsDao
}