package com.vladtop.test_application.data.api.DTO

import com.google.gson.annotations.SerializedName

data class OriginalDTO(
    @SerializedName("url") var url: String = ""
)