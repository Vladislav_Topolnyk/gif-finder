package com.vladtop.test_application.data.api.DTO

import com.google.gson.annotations.SerializedName

data class GifsDTO(
    @SerializedName("data") var data: ArrayList<DataDTO> = arrayListOf()
)
