package com.vladtop.test_application.di

import android.content.Context
import androidx.room.Room
import com.vladtop.test_application.data.room.dao.GifsDao
import com.vladtop.test_application.data.room.databases.GifsDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RoomModule {

    @Provides
    fun provideDatabaseDao(database: GifsDatabase): GifsDao = database.forecastDao()

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): GifsDatabase =
        Room.databaseBuilder(
            context, GifsDatabase::class.java,
            GifsDatabase.DB_NAME
        )
            .fallbackToDestructiveMigration()
            .build()
}