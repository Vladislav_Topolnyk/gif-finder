package com.vladtop.test_application.domain

import com.vladtop.test_application.data.room.entities.GifsEntity
import java.io.Serializable


data class Gif(
    val id: String,
    val url: String,
    var q: String = "",
    val isRemoved: Boolean = false
) : Serializable {

    fun toGifEntity() = GifsEntity(
        id = id,
        url = url,
        q = q,
        isRemoved = isRemoved,
        false
    )
}